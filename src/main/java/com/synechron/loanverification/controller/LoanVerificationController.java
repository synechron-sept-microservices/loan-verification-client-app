package com.synechron.loanverification.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authorization-code/callback")
public class LoanVerificationController {

    @GetMapping
    public ResponseEntity<String> greetUser(){
        return ResponseEntity.ok("test");
    }
}