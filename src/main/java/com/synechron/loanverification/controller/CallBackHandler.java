package com.synechron.loanverification.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/callback")
public class CallBackHandler {

    @GetMapping
    public String test(){
        return "Logged in User";
    }
}