package com.synechron.loanverification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoanVerificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoanVerificationApplication.class, args);
    }

}
